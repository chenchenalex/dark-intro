function coverTarget(mask, target) {
    var doc = document.documentElement,
        body = document.body,
        offsetTop = doc.scrollTop || body.scrollTop,
        offsetLeft = doc.scrollLeft || body.scrollLeft,
        pageWidth = doc.scrollWidth,
        pageHeight = doc.scrollHeight,
        targetPosition = target.getBoundingClientRect();

    // change target's styles
    mask.style.width = target.clientWidth + "px";
    mask.style.height = target.clientHeight + "px";
    mask.style.borderWidth = targetPosition.top + offsetTop + "px " + (pageWidth - offsetLeft - targetPosition.right) + "px " +
        (pageHeight - offsetTop - targetPosition.bottom) + "px " + targetPosition.left + "px ";

    mask.style.display = "block";

    // resize update
    if (!mask.isResizeBind) {
        if (window.addEventListener) {
            window.addEventListener("resize", function() {
                coverTarget(mask, targets[counter]);
            });
            mask.isResizeBind = true;
        } else if (window.attachEvent) {
            window.attachEvent("onresize", function() {
                coverTarget(mask, targets[counter]);
            });
            mask.isResizeBind = true;
        }
    }

}

var mask = document.getElementById('cover'),
    counter = 0;

var targets = [
    document.getElementById('target1'),
    document.getElementById('target2'),
    document.getElementById('target3'),
    document.getElementById('target4'),
    document.getElementById('target5'),
    document.getElementById('target6')
];

coverTarget(mask, targets[counter]);

var changeMusk = function() {
    counter++;
    if(!targets[counter]){
    	counter = 0;
    }
    coverTarget(mask, targets[counter]);
};

document.addEventListener("click", changeMusk);

document.addEventListener('keydown', function(e) {
    if (e.code == "Space")
        mask.style.display = "none";
    document.removeEventListener("click", changeMusk);
});
